﻿using System;

namespace Arrays
{
    public enum SortOrder { Ascending, Descending };
    static public class Operations
    {
        public static bool IsSorted(int[] array, SortOrder order)
        {
            // UNCOMMENT THREE LINES BELOW
            //if (array == null)
            //    throw new ArgumentNullException();
            //else
            {
                if (order == SortOrder.Ascending)
                {
                    for (int i = 0; i < array.Length - 1; i++)
                    {
                        if (array[i] > array[i + 1])
                            return false;
                    }
                    return true;
                }
                else
                {
                    for (int i = 0; i < array.Length - 1; i++)
                    {
                        if (array[i] < array[i + 1])
                            return false;
                    }
                    return true;
                }
            }
        }
    }
}
